﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading;
using System.Xml;
using System.ServiceProcess;
using System.Text.RegularExpressions;

namespace golfsa
{
    public partial class Service1 : ServiceBase
    {
        public System.Timers.Timer sysTimer;
        public string dbConnString = ConfigurationManager.AppSettings["sql"];
        public SqlConnection dbConn;
        bool dbConnected = true;
        bool useProxy = Convert.ToBoolean(ConfigurationManager.AppSettings["useProxy"]);
        int runTime = Convert.ToInt32(ConfigurationManager.AppSettings["runTime"]);
        public string tour = ConfigurationManager.AppSettings["tour"];
        public string leaderBoardEndpoint = ConfigurationManager.AppSettings["leaderBoardEndpoint"];
        runInfo curRun;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        protected override void OnStop()
        {

        }

        private void dbConnChanged(object sender, EventArgs e)
        {
            if (dbConnected && dbConn != null)
            {
                if (dbConn.State == ConnectionState.Broken || dbConn.State == ConnectionState.Closed)
                {
                    try
                    {
                        dbConn = new SqlConnection(dbConnString);
                        dbConn.Open();
                    }
                    catch (Exception ex)
                    {
                        dbConnected = false;
                    }
                }
            }
        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(run);
            t.IsBackground = true;
            t.Start();
        }

        private void run()
        {
            curRun = new runInfo();
            curRun.Start = DateTime.Now;
            curRun.Errors = new List<string>();

            try
            {
                dbConn = new SqlConnection(dbConnString);
                dbConn.Open();
            }
            catch (Exception ex)
            {
                dbConnected = false;
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }

            if (dbConnected && dbConn != null)
            {
                try
                {
                    dbConn.StateChange += new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }

                try
                {
                    dbConn.StateChange -= new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }
            }
            else
            {
                curRun.Errors.Add("Failed to connect to the database");
            }

            string leaderboardXml = getLeaderboard();
            XmlDocument xmlDoc = null;
            if (!String.IsNullOrEmpty(leaderboardXml))
            {
                xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(leaderboardXml);
                }
                catch (Exception ex)
                {
                    xmlDoc = null;
                    curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }
            }
            
            if (xmlDoc != null)
            {
                Leaderboard(xmlDoc, false);
            }

            //string scorecardXml = getScorecard();
            //xmlDoc = null;
            //if (!String.IsNullOrEmpty(scorecardXml))
            //{
            //    xmlDoc = new XmlDocument();
            //    try
            //    {
            //        xmlDoc.LoadXml(scorecardXml);
            //    }
            //    catch (Exception ex)
            //    {
            //        xmlDoc = null;
            //        curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
            //    }
            //}

            //if (xmlDoc != null)
            //{
            //    Scorecard(xmlDoc);
            //}

            curRun.End = DateTime.Now;
            endRun();
            writeInfo();

            if (dbConn != null)
            {
                dbConn.Dispose();
            }

            sysTimer = new System.Timers.Timer(runTime);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        public string getLeaderboard()
        {
            string xml = string.Empty;

            try
            {
                WebRequest myRequest = WebRequest.Create(leaderBoardEndpoint);
                if (useProxy)
                {
                    myRequest.Proxy = returnProxy();
                }
                myRequest.Timeout = 3600000;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                xml = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                xml = string.Empty;
            }

            return xml;
        }

        public string getScorecard()
        {
            string xml = string.Empty;

            try
            {
                WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=RSyndScorecard&action=Request&u=supersportfeed&p=gjg093j4trt49");
                if (useProxy)
                {
                    myRequest.Proxy = returnProxy();
                }
                myRequest.Timeout = 3600000;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                xml = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();

                xml = xml.Replace("<?xml version=\"1.0\" encoding=\"windows-1252\"?>", "<?xml version=\"1.0\"?>");
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                xml = string.Empty;
            }

            return xml;
        }

        public void Leaderboard(XmlDocument xmlDoc, bool isTest)
        {
            SqlCommand sqlQuery;
            StringBuilder sb;
            string tempPlayer = string.Empty;

            if (isTest)
            {
                try
                {
                    tour = "sa-golf";
                    //dbConn = new SqlConnection("Server=10.16.208.128,55540;UID=tlns;PWD=pr0v1dence;DATABASE=golf");
                    dbConn = new SqlConnection("Server=10.10.4.40;UID=supersms;PWD=5m5RrR4;DATABASE=golf");
                    dbConn.Open();
                }
                catch (Exception ex)
                {
                    dbConnected = false;
                    curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }
            }

            try
            {
                XmlNodeList xmlEvents = xmlDoc.GetElementsByTagName("tm_tourn");
                foreach (XmlNode EventNode in xmlEvents)
                {
                    DateTime updated = new DateTime();
                    string tournId = string.Empty;
                    string tournament = string.Empty;
                    string location = string.Empty;
                    string country = string.Empty;
                    int round = 0;
                    string status = string.Empty;
                    string type = string.Empty;

                    string course = string.Empty;
                    string courseName = string.Empty;
                    int courseLengthYards = 0;
                    string courseLengthYardsString = string.Empty;
                    int courseLengthMetres = 0;
                    string courseLengthMetresString = string.Empty;
                    int courseFrontNineLengthYards = 0;
                    int courseBackNineLengthYards = 0;
                    int courseFrontNineLengthMetres = 0;
                    int courseBackNineLengthMetres = 0;
                    int coursePar = 0;
                    string courseParsString = string.Empty;
                    int courseFrontNinePar = 0;
                    int courseBackNinePar = 0;
                    int courseHoles = 18;

                    foreach (XmlNode DetailNode in EventNode.ChildNodes)
                    {
                        switch (DetailNode.Name)
                        {
                            case "full_name":
                                tournament = DetailNode.InnerText;
                                break;
                            case "code":
                                tournId = DetailNode.InnerText;
                                break;
                            case "last_scores_update_date":
                                string[] date = DetailNode.InnerText.Split('/');
                                updated = new DateTime(Convert.ToInt32(date[2]) + 2000, Convert.ToInt32(date[1]), Convert.ToInt32(date[0]), 0, 0, 0);
                                break;
                            case "last_scores_update_time":
                                if (DetailNode.InnerText.IndexOf(":") >= 0)
                                {
                                    string[] tmpTime = DetailNode.InnerText.Split(':');
                                    updated = new DateTime(updated.Year, updated.Month, updated.Day, Convert.ToInt32(tmpTime[0]), Convert.ToInt32(tmpTime[1]), 0);
                                }
                                break;
                            case "course_name":
                                location = DetailNode.InnerText;
                                courseName = DetailNode.InnerText;
                                break;
                            case "course_country":
                                country = DetailNode.InnerText;
                                break;
                            case "rounds_played":
                                if (!String.IsNullOrEmpty(DetailNode.InnerText))
                                {
                                    bool result = Int32.TryParse(DetailNode.InnerText, out round);
                                }
                                break;
                            case "scores_type":
                                type = DetailNode.InnerText;
                                break;
                            case "round_head":
                                status = DetailNode.InnerText;
                                break;
                            case "course_code":
                                course = DetailNode.InnerText;
                                break;
                            case "courseName":
                                course = DetailNode.InnerText;
                                break;
                            case "course_length_yards":
                                courseLengthYardsString = DetailNode.InnerText;
                                if (courseLengthYardsString.IndexOf(",") >= 0)
                                {
                                    string[] holeLengths = courseLengthYardsString.Split(',');
                                    if (holeLengths.Length >= 17)
                                    {
                                        int holeCount = 1;
                                        foreach (string holeLength in holeLengths)
                                        {
                                            if (!String.IsNullOrEmpty(holeLength))
                                            {
                                                if (holeCount < 9)
                                                {
                                                    courseFrontNineLengthYards += Convert.ToInt32(holeLength);
                                                }
                                                if (holeCount > 8)
                                                {
                                                    courseBackNineLengthYards += Convert.ToInt32(holeLength);
                                                }
                                                courseLengthYards += Convert.ToInt32(holeLength);
                                            }

                                            holeCount += 1;
                                        }
                                    }
                                }
                                break;
                            case "course_length_meters":
                                courseLengthMetresString = DetailNode.InnerText;
                                break;
                            case "course_total_length":
                                if (!String.IsNullOrEmpty(DetailNode.InnerText))
                                {
                                    bool result = Int32.TryParse(DetailNode.InnerText, out courseLengthMetres);
                                }
                                break;
                            case "course_out_length":
                                if (!String.IsNullOrEmpty(DetailNode.InnerText))
                                {
                                    bool result = Int32.TryParse(DetailNode.InnerText, out courseBackNineLengthMetres);
                                }
                                break;
                            case "course_in_length":
                                if (!String.IsNullOrEmpty(DetailNode.InnerText))
                                {
                                    bool result = Int32.TryParse(DetailNode.InnerText, out courseFrontNineLengthMetres);
                                }
                                break;
                            case "course_in_par":
                                if (!String.IsNullOrEmpty(DetailNode.InnerText))
                                {
                                    bool result = Int32.TryParse(DetailNode.InnerText, out courseFrontNinePar);
                                }
                                break;
                            case "course_par":
                                courseParsString = DetailNode.InnerText;
                                break;
                            case "course_out_par":
                                if (!String.IsNullOrEmpty(DetailNode.InnerText))
                                {
                                    bool result = Int32.TryParse(DetailNode.InnerText, out courseBackNinePar);
                                }
                                break;
                            case "course_total_par":
                                if (!String.IsNullOrEmpty(DetailNode.InnerText))
                                {
                                    bool result = Int32.TryParse(DetailNode.InnerText, out coursePar);
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    if (status.ToLower().IndexOf("final") >= 0)
                    {
                        status = "Completed";
                    }
                    else if (status.ToLower().IndexOf("latest") >= 0)
                    {
                        status = "In Progress";
                    }
                    else if (status.ToLower().IndexOf("end of") >= 0)
                    {
                        status = "End Of Day";
                    }
                    else
                    {
                        status = "";
                    }

                    status = FixStatus(status);

                    sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboardCourse WHERE (tour = @tour AND tournament = @tournament AND year = @year AND course = @course)", dbConn);
                    sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                    sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                    sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                    sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                    int coursePresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                    if (coursePresent <= 0)
                    {
                        sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboardCourse (tour, tournament, course, year, lengthMetres, lengthYards, frontNineLengthMetres, backNineLengthMetres, frontNineLengthYards, backNineLengthYards, name, shortName, par, frontNinePar, backNinePar, lastUpdated, holes) VALUES (@tour, @tournament, @course, @year, @lengthMetres, @lengthYards, @frontNineLengthMetres, @backNineLengthMetres, @frontNineLengthYards, @backNineLengthYards, @name, @shortName, @par, @frontNinePar, @backNinePar, @lastUpdated, @holes)", dbConn);
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = courseName;
                        sqlQuery.Parameters.Add("@shortName", SqlDbType.VarChar).Value = courseName;
                        sqlQuery.Parameters.Add("@lengthMetres", SqlDbType.Int).Value = courseLengthMetres;
                        sqlQuery.Parameters.Add("@lengthYards", SqlDbType.Int).Value = courseLengthYards;
                        sqlQuery.Parameters.Add("@frontNineLengthMetres", SqlDbType.Int).Value = courseFrontNineLengthMetres;
                        sqlQuery.Parameters.Add("@backNineLengthMetres", SqlDbType.Int).Value = courseBackNineLengthMetres;
                        sqlQuery.Parameters.Add("@frontNineLengthYards", SqlDbType.Int).Value = courseFrontNineLengthYards;
                        sqlQuery.Parameters.Add("@backNineLengthYards", SqlDbType.Int).Value = courseBackNineLengthYards;
                        sqlQuery.Parameters.Add("@par", SqlDbType.Int).Value = coursePar;
                        sqlQuery.Parameters.Add("@frontNinePar", SqlDbType.Int).Value = courseFrontNinePar;
                        sqlQuery.Parameters.Add("@backNinePar", SqlDbType.Int).Value = courseBackNinePar;
                        sqlQuery.Parameters.Add("@lastUpdated", SqlDbType.DateTime).Value = DateTime.Now;
                        sqlQuery.Parameters.Add("@holes", SqlDbType.Int).Value = courseHoles;
                        sqlQuery.ExecuteNonQuery();
                    }

                    sqlQuery = new SqlCommand("SELECT COUNT(Id) FROM golf.dbo.leaderboardCourseHoles WHERE (tour = @tour AND tournament = @tournament AND year = @year AND course = @course)", dbConn);
                    sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                    sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                    sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                    sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                    int Holes = Convert.ToInt32(sqlQuery.ExecuteScalar());

                    if (Holes < 18)
                    {
                        StringBuilder sbHoles = new StringBuilder();
                        string[] pars = courseParsString.Split(',');
                        string[] metres = courseLengthMetresString.Split(',');
                        string[] yards = courseLengthYardsString.Split(',');

                        for (int i = 0; i <= 17; i++)
                        {
                            int holePar = 0;
                            int holeMetres = 0;
                            int holeYards = 0;

                            if (pars.Length >= i && !String.IsNullOrEmpty(pars[i]))
                            {
                                holePar = Convert.ToInt32(pars[i]);
                            }
                            if (pars.Length >= i && !String.IsNullOrEmpty(metres[i]))
                            {
                                holeMetres = Convert.ToInt32(metres[i]);
                            }
                            if (pars.Length >= i && !String.IsNullOrEmpty(yards[i]))
                            {
                                holeYards = Convert.ToInt32(yards[i]);
                            }

                            if (holePar > 0)
                            {
                                sbHoles.Append("INSERT INTO golf.dbo.leaderboardCourseHoles (tour, tournament, course, year, hole, par, metres, yards,lastUpdated) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId + "', '" + course.Replace("'", "''") + "', " + DateTime.Now.Year + ", " + (i + 1) + ", " + holePar + ", " + holeMetres + ", " + holeYards + ", Getdate());");
                            }
                        }

                        if (!String.IsNullOrEmpty(sbHoles.ToString()))
                        {
                            sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardCourseHoles WHERE (tour = @tour AND tournament = @tournament AND year = @year AND course = @course)", dbConn);
                            sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                            sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                            sqlQuery.ExecuteNonQuery();

                            sqlQuery = new SqlCommand(sbHoles.ToString(), dbConn);
                            sqlQuery.ExecuteNonQuery();
                        }
                    }

                    sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboard WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                    sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                    sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                    sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                    int leaderboardPresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                    if (leaderboardPresent > 0)
                    {
                        sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard SET name = @name, location = @location, country = @country, round = @round, status = @status, updateDate = @updateDate, timeStamp = @timeStamp WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                    }
                    else
                    {
                        sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboard (tour, tournament, year, name, shortname, location, country, round, rounds, status, updateDate, timeStamp, type) VALUES (@tour, @tournament, @year, @name, @shortname, @location, @country, @round, @rounds, @status, @updateDate, @timeStamp, @type)", dbConn);
                    }
                    sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                    sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                    sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                    sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = tournament;
                    sqlQuery.Parameters.Add("@shortname", SqlDbType.VarChar).Value = tournament;
                    sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                    sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                    sqlQuery.Parameters.Add("@round", SqlDbType.Int).Value = round;
                    sqlQuery.Parameters.Add("@rounds", SqlDbType.Int).Value = 4;
                    sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                    sqlQuery.Parameters.Add("@updateDate", SqlDbType.DateTime).Value = DateTime.Now;
                    sqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = updated;
                    sqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = type;
                    sqlQuery.ExecuteNonQuery();

                    sb = new StringBuilder();
                    XmlNodeList xmlScores = EventNode.SelectNodes("scores/scores_entry");
                    foreach (XmlNode scoreNode in xmlScores)
                    {
                        string id = "0";
                        string firstname = string.Empty;
                        string lastname = string.Empty;
                        string knownAs = string.Empty;
                        string playerCountry = string.Empty;
                        int holesPlayed = 0;
                        string round1ToPar = string.Empty;
                        string round1Strokes = string.Empty;
                        string round2ToPar = string.Empty;
                        string round2Strokes = string.Empty;
                        string round3ToPar = string.Empty;
                        string round3Strokes = string.Empty;
                        string round4ToPar = string.Empty;
                        string round4Strokes = string.Empty;
                        int pos = 1000;
                        int tied = 0;
                        string total = string.Empty;
                        string playerStatus = string.Empty;
                        bool cut = false;
                        int sorting = 1;

                        foreach (XmlNode playerNode in scoreNode.ChildNodes)
                        {
                            int temp = 0;
                            bool tempResult;
                            string tmpValue = string.Empty;

                            switch (playerNode.Name)
                            {
                                case "pos":
                                    if (!String.IsNullOrEmpty(playerNode.InnerText))
                                    {
                                        bool result = Int32.TryParse(playerNode.InnerText, out pos);
                                    }
                                    break;
                                case "tied":
                                    if (!String.IsNullOrEmpty(playerNode.InnerText))
                                    {
                                        tied = 1;
                                    }
                                    break;
                                case "forename":
                                    firstname = playerNode.InnerText;
                                    if (lastname == "Els" && firstname == "Theodore")
                                        firstname = "Ernie";
                                    if (lastname == "Oosthuizen" && firstname == "Lodewicus")
                                        firstname = "Louis";
                                    break;
                                case "surname":
                                    lastname = playerNode.InnerText;
                                    tempPlayer = lastname;
                                    break;
                                case "known_as":
                                    knownAs = playerNode.InnerText;
                                    break;
                                case "nationality":
                                    playerCountry = playerNode.InnerText;
                                    break;
                                case "external_ref":
                                    id = playerNode.InnerText;
                                    if (String.IsNullOrEmpty(id))
                                    {
                                        id = "0";
                                    }

                                    string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ()";
                                    foreach (char c in id.ToCharArray())
                                    {
                                        if (characters.IndexOf(c.ToString()) >= 0)
                                        {
                                            id = id.Replace(c.ToString(), (characters.IndexOf(c.ToString()) + 1).ToString());
                                        }
                                    }

                                    break;
                                case "holes":
                                    if (!String.IsNullOrEmpty(playerNode.InnerText))
                                    {
                                        bool result = Int32.TryParse(playerNode.InnerText, out holesPlayed);
                                    }
                                    break;
                                case "score":
                                    total = cleanScore(playerNode.InnerText);
                                    break;
                                case "vspar":
                                    string tmpTotal = playerNode.InnerText;
                                    tempResult = Int32.TryParse(tmpTotal, out temp);
                                    if (tempResult)
                                    {
                                        total = tmpTotal;
                                    }
                                    else if (tmpTotal.ToLower() == "e" || tmpTotal.ToLower() == "par")
                                    {
                                        total = "E";
                                    }
                                    else if (tmpTotal.ToLower().IndexOf("-") >= 0 || tmpTotal.ToLower().IndexOf("+") >= 0)
                                    {
                                        total = tmpTotal;
                                    }
                                    else if (!String.IsNullOrEmpty(tmpTotal))
                                    {
                                        cut = true;
                                    }
                                    break;
                                case "score_R1":
                                    tmpValue = cleanScore(playerNode.InnerText);
                                    tempResult = Int32.TryParse(tmpValue, out temp);
                                    if (type == "P")
                                    {
                                        if (tempResult || !String.IsNullOrEmpty(tmpValue))
                                        {
                                            round1Strokes = tmpValue;
                                            round1ToPar = tmpValue;
                                        }
                                        else if (!tempResult && String.IsNullOrEmpty(playerNode.InnerText))
                                        {
                                            round1Strokes = "0";
                                            round1ToPar = "E";
                                        }
                                        else
                                        {
                                            cut = true;
                                        }
                                    }
                                    else
                                    {
                                        if (tempResult)
                                        {
                                            if (temp > 20)
                                            {
                                                round1Strokes = tmpValue;
                                                if (coursePar > 0)
                                                {
                                                    round1ToPar = (Convert.ToInt32(round1Strokes) - coursePar).ToString();
                                                    if (round1ToPar == "0")
                                                    {
                                                        round1ToPar = "E";
                                                    }
                                                    else if (round1ToPar.IndexOf("-") < 0)
                                                    {
                                                        round1ToPar = "+" + round1ToPar;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                round1ToPar = tmpValue;
                                            }
                                        }
                                        else if (!String.IsNullOrEmpty(tmpValue))
                                        {
                                            round1ToPar = tmpValue;
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(playerNode.InnerText))
                                            {
                                                cut = true;
                                            }
                                        }
                                    }
                                    break;
                                case "score_R2":
                                    tmpValue = cleanScore(playerNode.InnerText);
                                    tempResult = Int32.TryParse(tmpValue, out temp);
                                    if (type == "P")
                                    {
                                        if (tempResult || !String.IsNullOrEmpty(tmpValue))
                                        {
                                            round2Strokes = tmpValue;
                                            round2ToPar = tmpValue;
                                        }
                                        else if (!tempResult && String.IsNullOrEmpty(playerNode.InnerText))
                                        {
                                            round2Strokes = "0";
                                            round2ToPar = "E";
                                        }
                                        else
                                        {
                                            cut = true;
                                        }
                                    }
                                    else
                                    {
                                        if (tempResult)
                                        {
                                            if (temp > 20)
                                            {
                                                round2Strokes = tmpValue;
                                                if (coursePar > 0)
                                                {
                                                    round2ToPar = (Convert.ToInt32(round2Strokes) - coursePar).ToString();
                                                    if (round2ToPar == "0")
                                                    {
                                                        round2ToPar = "E";
                                                    }
                                                    else if (round2ToPar.IndexOf("-") < 0)
                                                    {
                                                        round2ToPar = "+" + round2ToPar;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                round2ToPar = tmpValue;
                                            }
                                        }
                                        else if (!String.IsNullOrEmpty(tmpValue))
                                        {
                                            round2ToPar = tmpValue;
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(playerNode.InnerText))
                                            {
                                                cut = true;
                                            }
                                        }
                                    }
                                    break;
                                case "score_R3":
                                    tmpValue = cleanScore(playerNode.InnerText);
                                    tempResult = Int32.TryParse(tmpValue, out temp);
                                    if (type == "P")
                                    {
                                        if (tempResult || !String.IsNullOrEmpty(tmpValue))
                                        {
                                            round3Strokes = tmpValue;
                                            round3ToPar = tmpValue;
                                        }
                                        else if (!tempResult && String.IsNullOrEmpty(playerNode.InnerText))
                                        {
                                            round3Strokes = "0";
                                            round3ToPar = "E";
                                        }
                                        else
                                        {
                                            cut = true;
                                        }
                                    }
                                    else
                                    {
                                        if (tempResult)
                                        {
                                            if (temp > 20)
                                            {
                                                round3Strokes = tmpValue;
                                                if (coursePar > 0)
                                                {
                                                    round3ToPar = (Convert.ToInt32(round3Strokes) - coursePar).ToString();
                                                    if (round3ToPar == "0")
                                                    {
                                                        round3ToPar = "E";
                                                    }
                                                    else if (round3ToPar.IndexOf("-") < 0)
                                                    {
                                                        round3ToPar = "+" + round3ToPar;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                round3ToPar = tmpValue;
                                            }
                                        }
                                        else if (!String.IsNullOrEmpty(tmpValue))
                                        {
                                            round3ToPar = tmpValue;
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(playerNode.InnerText))
                                            {
                                                cut = true;
                                            }
                                        }
                                    }
                                    break;
                                case "score_R4":
                                    tmpValue = cleanScore(playerNode.InnerText);
                                    tempResult = Int32.TryParse(tmpValue, out temp);
                                    if (type == "P")
                                    {
                                        if (tempResult || !String.IsNullOrEmpty(tmpValue))
                                        {
                                            round4Strokes = tmpValue;
                                            round4ToPar = tmpValue;
                                        }
                                        else if (!tempResult && String.IsNullOrEmpty(playerNode.InnerText))
                                        {
                                            round4Strokes = "0";
                                            round4ToPar = "E";
                                        }
                                        else
                                        {
                                            cut = true;
                                        }
                                    }
                                    else
                                    {
                                        if (tempResult)
                                        {
                                            if (temp > 20)
                                            {
                                                round4Strokes = tmpValue;
                                                if (coursePar > 0)
                                                {
                                                    round4ToPar = (Convert.ToInt32(round4Strokes) - coursePar).ToString();
                                                    if (round4ToPar == "0")
                                                    {
                                                        round4ToPar = "E";
                                                    }
                                                    else if (round4ToPar.IndexOf("-") < 0)
                                                    {
                                                        round4ToPar = "+" + round4ToPar;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                round4ToPar = tmpValue;
                                            }
                                        }
                                        else if (!String.IsNullOrEmpty(tmpValue))
                                        {
                                            round4ToPar = tmpValue;
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(playerNode.InnerText))
                                            {
                                                cut = true;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (holesPlayed == 0)
                        {
                            if ((round == 1 && !String.IsNullOrEmpty(round1Strokes)) || (round == 2 && !String.IsNullOrEmpty(round2Strokes)) || (round == 3 && !String.IsNullOrEmpty(round3Strokes)) || (round == 4 && !String.IsNullOrEmpty(round4Strokes)))
                            {
                                holesPlayed = 18;
                            }
                        }

                        if (String.IsNullOrEmpty(firstname))
                        {
                            cut = true;
                        }

                        if (cut == false)
                        {
                            sb.Append("INSERT INTO golf.dbo.leaderboardScores (tour, tournament, year, position, sorting, tied, playerId, firstName, lastName, knownAs, country, round1topar, round2topar, round3topar, round4topar, round1strokes, round2strokes, round3strokes, round4strokes, total, holesPlayed) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", " + pos + ", " + sorting + ", " + tied + ", '" + id + "', '" + firstname.Replace("'", "''") + "', '" + lastname.Replace("'", "''") + "', '" + knownAs.Replace("'", "''") + "', '" + playerCountry.Replace("'", "''") + "', '" + round1ToPar + "', '" + round2ToPar + "', '" + round3ToPar + "', '" + round4ToPar + "', '" + round1Strokes + "', '" + round2Strokes + "', '" + round3Strokes + "', '" + round4Strokes + "', '" + total + "', " + holesPlayed + ");");
                            sorting += 1;
                        }
                    }

                    if (!String.IsNullOrEmpty(sb.ToString()))
                    {
                        sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardScores WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        sqlQuery.ExecuteNonQuery();

                        sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                        sqlQuery.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorPlayer = tempPlayer;
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                string sda = ex.Message;
            }
        }

        private string cleanScore(string score)
        {
            string newScore = string.Empty;

            score = score.Replace("par", "E");
            score = score.Replace("Par", "E");
            score = score.Replace("PAR", "E");

            for (int i = 0; i <= score.Length - 1; i++)
            {
                string tmpChar = score.Substring(i, 1).ToLower();
                string actChar = score.Substring(i, 1);
                int temp = 0;
                bool result = Int32.TryParse(tmpChar, out temp);
                if (result)
                {
                    newScore += actChar;
                }
                if (tmpChar == "-")
                {
                    newScore += actChar;
                }
                else if (tmpChar == "+")
                {
                    newScore += actChar;
                }
                else if (tmpChar == "e")
                {
                    newScore += actChar;
                }
            }

            return newScore;
        }

        public void Scorecard(XmlDocument xmlDoc)
        {
            
        }

        private WebProxy returnProxy()
        {
            WebProxy tmpProxy = new WebProxy();
            FileStream Fs = new FileStream("C:\\SuperSport\\proxyDetails.txt", FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader Sw = new StreamReader(Fs);
            Sw.BaseStream.Seek(0, SeekOrigin.Begin);
            string proxyUsername = Sw.ReadLine();
            string proxyPassword = Sw.ReadLine();
            string proxyDomain = Sw.ReadLine();
            string proxyAddress = Sw.ReadLine();
            int proxyPort = Convert.ToInt32(Sw.ReadLine());
            Sw.Close();
            Fs.Close();
            Fs.Dispose();

            System.Net.NetworkCredential ProxyCredentials = new System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain);
            tmpProxy = new System.Net.WebProxy(proxyAddress, proxyPort);
            tmpProxy.Credentials = ProxyCredentials;

            return tmpProxy;
        }

        private void writeInfo()
        {
            try
            {
                FileStream Fs = new FileStream(ConfigurationManager.AppSettings["log"], FileMode.Create, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.BaseStream.Seek(0, SeekOrigin.Begin);
                Sw.WriteLine("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                foreach (string tmpString in curRun.Errors)
                {
                    Sw.WriteLine("Error: " + tmpString);
                }
                Sw.Close();
                Fs.Dispose();
            }
            catch (Exception ex)
            {

            }
        }

        private void endRun()
        {
            try
            {
                string errors = "";

                foreach (string tmpString in curRun.Errors)
                {
                    errors += "Error: " + tmpString + Environment.NewLine;
                }

                SqlCommand SqlQuery = new SqlCommand("Insert Into golf.dbo.leaderboardIngest (tournament, startTime, endTime, errors, timeStamp) Values (1, @startTime, @endTime, @errors, @timeStamp)", dbConn);
                SqlQuery.Parameters.Add("@startTime", SqlDbType.DateTime).Value = curRun.Start;
                SqlQuery.Parameters.Add("@endTime", SqlDbType.DateTime).Value = curRun.End;
                SqlQuery.Parameters.Add("@errors", SqlDbType.VarChar).Value = errors;
                SqlQuery.Parameters.Add("@timeStamp", SqlDbType.VarChar).Value = DateTime.Now;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                curRun.Errors.Add("Updating run details - " + ex.Message);
            }
        }

        private string FixStatus(string status)
        {
            string configOptions = ConfigurationManager.AppSettings["statuses"];
            string tempStatus = status.ToLower();

            Hashtable statuses = new Hashtable();
            string[] options = configOptions.Split(';');
            foreach (string option in options)
            {
                string[] values = option.Split('|');
                statuses.Add(values[0].ToString(), values[1].ToString());
            }

            if (statuses.ContainsKey(tempStatus))
            {
                status = statuses[tempStatus].ToString();
            }

            return status;
        }
    }
}